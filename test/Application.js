const assert = require('assert');
const { Application } = require('..');
const http = require('http');

describe('Application', function() {
	var app = new Application();
	const port = 9091;

	beforeEach(function() {
		app = new Application();
	});
	afterEach(function() {
		app.close();
	});
	function request(cb) {
		http.get(`http://localhost:${port}/`, (res) => {
			if(typeof cb !== 'undefined')
				cb(res);
		});
	}

	describe('#listen', function() {
		it('should be a function', function() {
			assert.equal(typeof app.listen, 'function');
		});
		it('should call callback on success', function(done) {
			app.listen(port, () => {
				done();
			});
		});
		it('should respond to http requests', function(done) {
			app.listen(port, () => {
				request((res) => {
					done();
				});
			});
		});
		it('should 404 when no body', function(done) {
			app.listen(port, () => {
				request((res) => {
					assert.equal(res.statusCode, 404);
					done();
				});
			});
		});
	});

	describe('#use', function() {
		it('should be a function', function() {
			assert.equal(typeof app.use, 'function');
		});
		it('should call middleware precisely once', function(done) {
			app.use(async () => {
				done();
			});
			app.listen(port, () => {
				request();
			});
		});
		it('should respond with data when appropriate', function(done) {
			app.use(async (ctx) => {
				ctx.res.body = 'stuff';
			});
			app.listen(port, () => {
				request((res) => {
					assert.equal(res.statusCode, 200);
					data = '';
					res.on('data', (chunk) => {
						data += chunk;
					}).on('end', () => {
						assert.equal(data, 'stuff');
						done();
					}).on('error', (e) => {
						done(e);
					});
				});
			});
		});
	});
});