'use strict';

const { Request } = require('./Request.js');
const { Response } = require('./Response.js');
const { HttpError } = require('./HttpErrors.js');

const debug = require('debug')('Application-Context');

class Context {
	constructor(request, response) {
		this.req = new Request(request);
		this.res = new Response(response);

		this.req.on('error', (e) => {
			this.error(e, 'req');
		});
		this.res.on('error', (e) => {
			this.error(e, 'res');
		});
	}
	error(err, where) {
		debug(`caught ${err} in ${where}`);

		if(err instanceof HttpError) {
			this.res.body = err.message ? `${err.name}: ${err.message}` : err.name;
			this.res.code = err.code;
		} else {
			this.res.body = 'Error';
			this.res.code = 500;
			debug(err);
		}
	}
}

module.exports = {
	Context,
};