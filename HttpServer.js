'use strict';

const { BaseServer } = require('./BaseServer.js');
const { Context } = require('./Context.js');

const http = require('http');
const debug = require('debug')('Application-Http');

async function cbs(server, ctx) {
	try {
		await server.cb(0, ctx)();
	} catch(err) {
		ctx.error(err, 'cbs');
	}
}

class HttpServer extends BaseServer {
	constructor() {
		super();
		this._server = http.createServer((request, response) => {
			const ctx = new Context(request, response);
			ctx.req.on('end', () => {
				cbs(this, ctx).then(() => {
					this.end(ctx);
				});
			});
		});
		this._server.on('error', (e) => {
			debug(e);
		});
	}
	listen(port, cb) {
		this._server.listen(port, cb);
	}
	close(cb) {
		this._server.close(cb);
	}
}

module.exports = {
	HttpServer,
};
