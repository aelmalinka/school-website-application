'use strict';

const errors = require('./HttpErrors.js');

class Response {
	constructor(res) {
		this.response = res;
		this.headers = {
			'Content-Type': 'text/plain',
		};
		this.code = 404;
	}
	on(event, f) {
		this.response.on(event, f);
	}
	write() {
		if(this.code >= 400) {
			throw errors.get(this.code);
		} else if(typeof this.body === 'undefined') {
			throw new errors.NotFound();
		} else if(typeof this.body === 'object') {
			this.body = JSON.stringify(this.body);
			this.headers['Content-Type'] = 'application/json';
		}

		this.end();
	}
	end() {
		this.response.writeHead(this.code, this.headers);
		if(this.body)
			this.response.end(this.body);
		else
			this.response.end();
	}
	get code() {
		return this._code;
	}
	get body() {
		return this._body;
	}
	set code(value) {
		this._code = value;
	}
	set body(value) {
		if(this._code == 404)
			this._code = 200;
		this._body = value;
	}
}

module.exports = {
	Response,
};