'use strict';

const debug = require('debug')('Application-BaseServer');

class BaseServer {
	constructor() {
		this.cbs = [];
	}
	use(f) {
		this.cbs.push(f);
	}
	end(ctx) {
		debug(`${ctx.req.url} ${ctx.res.code}`);
		try {
			ctx.res.write();
		} catch(err) {
			ctx.res.end();
		}
	}
	cb(x, ctx) {
		let done = false;
		return async () => {
			if(!done) {
				if(x < this.cbs.length) {
					const next = this.cb(x + 1, ctx);
					await this.cbs[x](ctx, next);
					await next();
				}
				done = true;
			}
		};
	}
}

module.exports = {
	BaseServer,
};