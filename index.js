'use strict';

const { HttpServer } = require('./HttpServer.js');
const errors = require('./HttpErrors.js');

class Application extends HttpServer {
}

module.exports = {
	Application,
};

for(const error in errors) {
	if(typeof error !== 'undefined' && error !== 'get')
		eval(`module.exports.${error} = errors.${error}`);
}
