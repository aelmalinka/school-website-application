'use strict';

class HttpError extends Error {
	constructor(message, code = 500, name = 'Server Error') {
		super(message);
		this.code = code;
		this.name = name;
	}
}

class BadRequest extends HttpError {
	constructor(message) {
		super(message, 400, 'Bad Request');
	}
}

class NotAuthenticated extends HttpError {
	constructor(message) {
		super(message, 401, 'Not Authenticated');
	}
}

class NotAuthorized extends HttpError {
	constructor(message) {
		super(message, 403, 'Not Authorized');
	}
}

class NotFound extends HttpError {
	constructor(message) {
		super(message, 404, 'Not Found');
	}
}

class Conflict extends HttpError {
	constructor(message) {
		super(message, 409, 'Conflict');
	}
}

class NotImplemented extends HttpError {
	constructor(message) {
		super(message, 501, 'Not Implemented');
	}
}

function get(code) {
	switch(code) {
		case 401: return new NotAuthenticated();
		case 403: return new NotAuthorized();
		case 404: return new NotFound();
		case 409: return new Conflict();
		case 500: return new HttpError();
		case 501: return new NotImplemented();
	}
}

module.exports = {
	HttpError,
	BadRequest,
	NotAuthorized,
	NotAuthenticated,
	NotFound,
	NotImplemented,
	Conflict,
	get,
};
