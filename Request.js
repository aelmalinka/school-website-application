'use strict';

const debug = require('debug')('Application-Request');

class Request {
	constructor(req) {
		this.request = req;
		this.headers = req.headers;
		this.method = req.method;
		this.url = req.url;
		this.body = [];

		this.on('data', (chunk) => {
			this.body.push(chunk);
		}).on('end', () => {
			this.body = decodeURI(Buffer.concat(this.body).toString());
			try {
				this.body = JSON.parse(this.body);
			} catch(e) {
				debug(`json parse error: ${e} on '${this.body}'`);
			}
		});
	}
	on(event, f) {
		return this.request.on(event, f);
	}
}

module.exports = {
	Request,
};